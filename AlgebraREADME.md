# Davor Orešić - ReactJS Algebra Seminarski rad - Chat App

Ovo je projekt napravljen da radi sa [Scaledrone Chat](https://www.scaledrone.com).
Korištena tehnologija sa uputama: [Scaledrone JavaScript API](https://www.scaledrone.com/docs/api-clients/javascript)

Sve datoteke potrebne za seminarski rad nalaze se ovdje na repozitoriju:

https://gitlab.com/algebra_fedev/doresic-reactjs-seminarski


## Minimalni uvjeti funkcionalne specifikacije:
1. Kod postaviti na Git repozitorij
2. Chat aplikacija bi trebala:
    - moći kreirati nove tekstualne poruke   *izrađeno*
    - poslati poruku uz ime autora           *izrađeno*
    - za svakog sudionika u chatu izabrati random boju  *izrađeno* (ili ime)
    - povezati se sa scaledrone servisom    *izrađeno*
    - biti dostupna preko Git repozitorija (GitHub, GitLab) *izrađeno*

Svi gornji uvjeti su sa trenutnom verzijom aplikacije zadovoljeni.
