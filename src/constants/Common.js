export const CHANNEL_ID  = "9eXhz3Mk3q8QYUPc";
export const CHATNAME    = randomChatname(); //"doresic";    //  ime u chatu
export const COLOR       = randomColor(); //"#aabbcc"; //   random boja za svakog korisnika u chatu
export const ROOM_NAME   = "observable-room";

function randomColor() {
    return '#' + Math.floor(Math.random() * 0xFFFFFF).toString(16);
}

function randomChatname() {
    return 'user' + Math.floor( Math.random() * 1000 );
}
