import React, { Component } from 'react'; 

import '../css/App.css';
import Messages         from "./Messages";
import Input            from "./Input";

import { CHANNEL_ID }   from "../constants/Common";
import { ROOM_NAME }    from "../constants/Common";
import { CHATNAME }     from "../constants/Common";
import { COLOR }        from "../constants/Common";


export default class App extends Component {
  state = {
    messages: [],
    member: {
      username: CHATNAME,
      color:    COLOR,
    }
  }

  constructor() {
    super();
    this.chat = new window.Scaledrone(CHANNEL_ID, {
      data: this.state.member
    });

    this.chat.on('open', error => {
      if (error) {
        return console.error(error);
      }
      const member = {...this.state.member};
      member.id = this.chat.clientId;
      this.setState({member});
    });
    const room = this.chat.subscribe(ROOM_NAME);
    room.on('data', (data, member) => {
      const messages = this.state.messages;
      messages.push({member, text: data});
      this.setState({messages});
    });
  }

  render() {
    return (
      <div className="flex-container">
        <div className="header">
          <h1>Chat</h1>
        </div>
        <main>
        <Messages
          messages={this.state.messages}
          currentMember={this.state.member}
          />
        </main>
        <div className="footer">
        <Input 
          onSendMessage={this.onSendMessage}
          />
        </div>
      </div>
    );
  }

  onSendMessage = (message) => {
    if (message.length > 0)  
    {
      this.chat.publish({
        room: ROOM_NAME,
        message
      });
    }
  }

}
