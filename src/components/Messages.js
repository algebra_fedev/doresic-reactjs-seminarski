import {Component} from "react";
import React from "react";


export default class Messages extends Component {
  render() {
    const {messages} = this.props;
    return (
      <ul className="Messages-list">
        {messages.map(m => this.renderMessage(m))}
      </ul>
    );
  }

  renderMessage(message) {
    const {member, text} = message;
    const {currentMember} = this.props;
    const messageFromMe = member.id === currentMember.id;
    const className = messageFromMe ?
      "Messages-message currentMember" : "Messages-message";
    return (
      <li key={Math.random()} className={className}>
        <div className="Message-content">
          <div className="text, blocks">{text}</div>
          <div className="avatar, blocks" style={{ backgroundColor: member.clientData.color }}>
            {member.clientData.username}
          </div>
        </div>
      </li>
    );
  }
}

